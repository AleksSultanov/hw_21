from flask import Flask, request
import requests
import os


app = Flask(__name__)

@app.route("/")
def home():
    # последний номер порта  
    _numport = request.host[-1::1]
    # ip следующего API
    _ip = request.host[:-1]+str(int(_numport)+1)
    res = ""
    try:
        response = requests.get('http://'+_ip)
        res = '=>'+response.text
    except:
        res = ""
    return _numport+ res

if __name__ == "__main__":
    # запуск по определенному порту
    app.run(host='0.0.0.0', port=os.getenv('API_PORT'))